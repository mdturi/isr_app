var gulp = require('gulp'),
  less = require('gulp-less'),
  cssmin = require('gulp-cssmin'),
  rename = require('gulp-rename'),
  concatCss = require('gulp-concat-css'),
  clean = require('gulp-clean'),
  put = require('gulp-put');

var css_path = {src: './src/css/', dist: './dist/css/' };
var assets_path = {src: './src/assets/', dist: './dist/assets/' };
var app_path = {src: './src/app/', dist: './dist/app/' };
var js_path = {src: './src/js/', dist: './dist/js/' };

/* dev - watch */
gulp.task('less', function () {
   return gulp.src(css_path.src + '*.less')
     .pipe(less().on('error', function (err) {
        console.log(err);
     }))
     .pipe(gulp.dest(css_path.src));

});
gulp.task('watch', function(){
   gulp.watch(css_path.src + '*.less', ['less']);
});
/* /dev - watch */

/* build */
gulp.task('clean-css', function(){
  //clean
  return gulp.src(css_path.dist + '*.*', {read: false})
    .pipe(clean());
});
gulp.task('css-concat', ['clean-css'], function () {
   return gulp.src(css_path.src + '*.css')
     .pipe(concatCss('styles.css'))
     .pipe(gulp.dest(css_path.dist));

});
gulp.task('css-minify',['css-concat'], function () {
   return gulp.src(css_path.dist + '*.css')
     .pipe(cssmin())
     .pipe(rename({suffix: '.min'}))
     .pipe(gulp.dest(css_path.dist));

});
gulp.task('_assets', function () {
  return gulp.src([ assets_path.src  + '*.*'])
    .pipe(put('./dist'));

});
gulp.task('assets', function () {
  return gulp.src([ assets_path.src  + '*.*'])
    .pipe(gulp.dest(assets_path.dist));

});
gulp.task('app', function () {
  return gulp.src([ app_path.src  + '*.*'])
    .pipe(gulp.dest(app_path.dist));

});
gulp.task('clean-js', function(){
  //clean
  return gulp.src(js_path.dist + '*.*', {read: false})
    .pipe(clean());
});
gulp.task('js-minify', ['clean-js'], function(){
  //clean
  return gulp.src(js_path.dist + '*.js', {read: false})
    .pipe(clean());
});
/* /build */


gulp.task('default', ['less', 'watch']);
gulp.task('build', ['css-minify', 'assets', 'app', 'js-minify']);
